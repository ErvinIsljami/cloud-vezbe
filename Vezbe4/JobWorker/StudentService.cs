﻿using Contracts;
using StudentServiceData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobWorker
{
    public class StudentService : IStudentService
    {
        StudentDataRepository repo = new StudentDataRepository();
        public void AddStudent(string indexNo, string name, string lastName)
        {
            Student s = new Student(indexNo);
            s.Name = name;
            s.LastName = lastName;
            repo.AddStudent(s);
        }

        public List<Student> RetrieveAllStudents()
        {
            return repo.RetrieveAllStudents().ToList();
        }
    }
}
