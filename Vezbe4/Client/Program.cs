﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Contracts;
using StudentServiceData;

namespace Client
{
    class Program
    {
        private static IStudentService proxy;
        static void Main(string[] args)
        {
            ChannelFactory<IStudentService> chf = new ChannelFactory<IStudentService>(new NetTcpBinding(), string.Format(@"net.tcp://localhost:10100/StudentService"));
            proxy = chf.CreateChannel();

            while(true)
            {
                int i = 0;
                Console.WriteLine("Unesite izbor");
                Console.WriteLine("1. Unos studenta");
                Console.WriteLine("2. Ispis svih studenata");
                i = Int32.Parse(Console.ReadLine());
                if(i == 1)
                {
                    string ime;
                    string prezime;
                    string indeks;
                    Console.WriteLine("Unesite ime");
                    ime = Console.ReadLine();
                    Console.WriteLine("Unesite prezime");
                    prezime = Console.ReadLine();
                    Console.WriteLine("Unesite indeks");
                    indeks = Console.ReadLine();
                    proxy.AddStudent(indeks, ime, prezime);
                }
                else if(i == 2)
                {
                    List<Student> lista = proxy.RetrieveAllStudents();
                    Console.WriteLine("Svi studenti: ");
                    foreach(Student s in lista)
                    {
                        Console.WriteLine(s.Name + " " + s.LastName);
                    }
                }
                else
                {
                    break;
                }
            }


            Console.WriteLine("Pritisnite enter za izlazak");
            Console.ReadLine();



        }
    }
}
