﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="DistributedTransaction" generation="1" functional="0" release="0" Id="bc99bf6d-bc76-4091-a19a-1c229fb05947" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="DistributedTransactionGroup" generation="1" functional="0" release="0">
      <settings>
        <aCS name="Bank:DataConnectionString2" defaultValue="">
          <maps>
            <mapMoniker name="/DistributedTransaction/DistributedTransactionGroup/MapBank:DataConnectionString2" />
          </maps>
        </aCS>
        <aCS name="Bank:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/DistributedTransaction/DistributedTransactionGroup/MapBank:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="BankInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/DistributedTransaction/DistributedTransactionGroup/MapBankInstances" />
          </maps>
        </aCS>
        <aCS name="Bookstore:DataConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/DistributedTransaction/DistributedTransactionGroup/MapBookstore:DataConnectionString" />
          </maps>
        </aCS>
        <aCS name="Bookstore:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/DistributedTransaction/DistributedTransactionGroup/MapBookstore:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="BookstoreInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/DistributedTransaction/DistributedTransactionGroup/MapBookstoreInstances" />
          </maps>
        </aCS>
        <aCS name="TransactionCoordinator:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/DistributedTransaction/DistributedTransactionGroup/MapTransactionCoordinator:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="TransactionCoordinatorInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/DistributedTransaction/DistributedTransactionGroup/MapTransactionCoordinatorInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <sFSwitchChannel name="SW:Bank:InternalEndpoint">
          <toPorts>
            <inPortMoniker name="/DistributedTransaction/DistributedTransactionGroup/Bank/InternalEndpoint" />
          </toPorts>
        </sFSwitchChannel>
        <sFSwitchChannel name="SW:Bookstore:InternalEndpoint">
          <toPorts>
            <inPortMoniker name="/DistributedTransaction/DistributedTransactionGroup/Bookstore/InternalEndpoint" />
          </toPorts>
        </sFSwitchChannel>
      </channels>
      <maps>
        <map name="MapBank:DataConnectionString2" kind="Identity">
          <setting>
            <aCSMoniker name="/DistributedTransaction/DistributedTransactionGroup/Bank/DataConnectionString2" />
          </setting>
        </map>
        <map name="MapBank:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/DistributedTransaction/DistributedTransactionGroup/Bank/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapBankInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/DistributedTransaction/DistributedTransactionGroup/BankInstances" />
          </setting>
        </map>
        <map name="MapBookstore:DataConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/DistributedTransaction/DistributedTransactionGroup/Bookstore/DataConnectionString" />
          </setting>
        </map>
        <map name="MapBookstore:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/DistributedTransaction/DistributedTransactionGroup/Bookstore/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapBookstoreInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/DistributedTransaction/DistributedTransactionGroup/BookstoreInstances" />
          </setting>
        </map>
        <map name="MapTransactionCoordinator:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/DistributedTransaction/DistributedTransactionGroup/TransactionCoordinator/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapTransactionCoordinatorInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/DistributedTransaction/DistributedTransactionGroup/TransactionCoordinatorInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="Bank" generation="1" functional="0" release="0" software="C:\Users\Ervin\source\repos\DistributedTransaction\DistributedTransaction\csx\Debug\roles\Bank" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="InternalEndpoint" protocol="tcp" />
              <outPort name="Bank:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/DistributedTransaction/DistributedTransactionGroup/SW:Bank:InternalEndpoint" />
                </outToChannel>
              </outPort>
              <outPort name="Bookstore:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/DistributedTransaction/DistributedTransactionGroup/SW:Bookstore:InternalEndpoint" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="DataConnectionString2" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;Bank&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;Bank&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;Bookstore&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;TransactionCoordinator&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/DistributedTransaction/DistributedTransactionGroup/BankInstances" />
            <sCSPolicyUpdateDomainMoniker name="/DistributedTransaction/DistributedTransactionGroup/BankUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/DistributedTransaction/DistributedTransactionGroup/BankFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="Bookstore" generation="1" functional="0" release="0" software="C:\Users\Ervin\source\repos\DistributedTransaction\DistributedTransaction\csx\Debug\roles\Bookstore" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="InternalEndpoint" protocol="tcp" />
              <outPort name="Bank:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/DistributedTransaction/DistributedTransactionGroup/SW:Bank:InternalEndpoint" />
                </outToChannel>
              </outPort>
              <outPort name="Bookstore:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/DistributedTransaction/DistributedTransactionGroup/SW:Bookstore:InternalEndpoint" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="DataConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;Bookstore&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;Bank&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;Bookstore&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;TransactionCoordinator&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/DistributedTransaction/DistributedTransactionGroup/BookstoreInstances" />
            <sCSPolicyUpdateDomainMoniker name="/DistributedTransaction/DistributedTransactionGroup/BookstoreUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/DistributedTransaction/DistributedTransactionGroup/BookstoreFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="TransactionCoordinator" generation="1" functional="0" release="0" software="C:\Users\Ervin\source\repos\DistributedTransaction\DistributedTransaction\csx\Debug\roles\TransactionCoordinator" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <componentports>
              <outPort name="Bank:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/DistributedTransaction/DistributedTransactionGroup/SW:Bank:InternalEndpoint" />
                </outToChannel>
              </outPort>
              <outPort name="Bookstore:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/DistributedTransaction/DistributedTransactionGroup/SW:Bookstore:InternalEndpoint" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;TransactionCoordinator&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;Bank&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;Bookstore&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;TransactionCoordinator&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/DistributedTransaction/DistributedTransactionGroup/TransactionCoordinatorInstances" />
            <sCSPolicyUpdateDomainMoniker name="/DistributedTransaction/DistributedTransactionGroup/TransactionCoordinatorUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/DistributedTransaction/DistributedTransactionGroup/TransactionCoordinatorFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="TransactionCoordinatorUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="BankUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="BookstoreUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="BankFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="BookstoreFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="TransactionCoordinatorFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="BankInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="BookstoreInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="TransactionCoordinatorInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
</serviceModel>