﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterroleContracts;

namespace JobWorker
{
    public class Job : IJob
    {
        public int DoCalculus(int to)
        {
            Trace.TraceInformation("Do calculus method called[1, {0}]", to);
            int rez = 0;
            for(int i = 1; i <= to; i++)
            {
                rez += i;
            }
            return rez;
        }
    }
}
