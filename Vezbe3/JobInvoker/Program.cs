﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterroleContracts;
using System.ServiceModel;

namespace JobInvoker
{
    class Program
    {
        static void Main(string[] args)
        {
            IJob proxy;
            ChannelFactory<IJob> chf = new ChannelFactory<IJob>(new NetTcpBinding(), string.Format("net.tcp://localhost:10100/InputRequest"));

            proxy = chf.CreateChannel();

            while(true)
            {
                Console.WriteLine("Unesite gornju granicu");
                Console.WriteLine("rez = " + proxy.DoCalculus(Int32.Parse(Console.ReadLine())));
            }


            Console.ReadLine();
        }
    }
}
