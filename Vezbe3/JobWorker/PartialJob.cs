﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InterroleContracts;

namespace JobWorker
{
    public class PartialJob : IPartialJob
    {
        public int DoSum(int from, int to)
        {
            int ret = 0;
            for (int i = from; i < to; i++)
            {
                ret += i;
            }
            Trace.TraceInformation("Parcijalna zavrsena: " + ret);
            return ret;
        }
    }
}
