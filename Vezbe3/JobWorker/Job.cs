﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using InterroleContracts;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace JobWorker
{
    public class Job : IJob
    {
        public int DoCalculus(int to)
        {
            Trace.TraceInformation("Do calculus method called[1, {0}]", to);
            int rez = 0;
            int numberOfBrothers = RoleEnvironment.Roles["JobWorker"].Instances.Count - 1;
            Task<int>[] taskovi = new Task<int>[numberOfBrothers];

            int cnt = 0;
            int cnt2 = 0;
            int duzina = (to + 1) / numberOfBrothers;
            int lower = 0;
            int upper;
            int i = 0;
            Dictionary<string, Tuple<int, int>> intervali = new Dictionary<string, Tuple<int, int>>();
            foreach(RoleInstance instance in RoleEnvironment.Roles["JobWorker"].Instances)
            {
                if(instance != RoleEnvironment.CurrentRoleInstance)
                {
                    upper = (lower + duzina) % to;
                    if (i + 1 == numberOfBrothers) 
                        upper = to + 1;

                    intervali.Add(instance.Id, new Tuple<int, int>(lower, upper));
                    lower = upper;
                    i++;
                }
            }


            foreach (RoleInstance instance in RoleEnvironment.Roles["JobWorker"].Instances)
            {
                if (instance != RoleEnvironment.CurrentRoleInstance)
                {
                    Task<int> taskObjekat = new Task<int>(() =>
                    {

                        RoleInstanceEndpoint inputEndPoint = RoleEnvironment.CurrentRoleInstance.InstanceEndpoints["InternalRequest"];
                        string endpoint = String.Format("net.tcp://{0}/InternalRequest", inputEndPoint.IPEndpoint);
                        string instanceName = instance.Id;
                        int part = 0;
                        IPartialJob proxy;
                        ChannelFactory<IPartialJob> chf = new ChannelFactory<IPartialJob>(new NetTcpBinding(), endpoint);
                        proxy = chf.CreateChannel();

                        part = proxy.DoSum(intervali[instance.Id].Item1, intervali[instance.Id].Item2);
                        cnt2++;
                        Trace.TraceInformation(string.Format("Instance{0} = {1}", instanceName, part));
                        return part;
                    });
                    taskObjekat.Start();
                    taskovi[cnt++] = taskObjekat;
                    
                }
            }
            Task.WaitAll(taskovi);
            foreach(Task<int> t in taskovi)
            {
                rez += t.Result;
            }


            return rez;
        }
    }
}
