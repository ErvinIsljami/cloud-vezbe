﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="WCFHealthMonitoring" generation="1" functional="0" release="0" Id="76f58eaa-4f0d-4f57-86de-bb5b2e17b5c0" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="WCFHealthMonitoringGroup" generation="1" functional="0" release="0">
      <settings>
        <aCS name="HealthMonitor:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/WCFHealthMonitoring/WCFHealthMonitoringGroup/MapHealthMonitor:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="HealthMonitorInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/WCFHealthMonitoring/WCFHealthMonitoringGroup/MapHealthMonitorInstances" />
          </maps>
        </aCS>
      </settings>
      <maps>
        <map name="MapHealthMonitor:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/WCFHealthMonitoring/WCFHealthMonitoringGroup/HealthMonitor/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapHealthMonitorInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/WCFHealthMonitoring/WCFHealthMonitoringGroup/HealthMonitorInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="HealthMonitor" generation="1" functional="0" release="0" software="C:\Users\Ervin\Documents\Cloud\Vezbe1\WCFHealthMonitoring\csx\Debug\roles\HealthMonitor" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;HealthMonitor&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;HealthMonitor&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/WCFHealthMonitoring/WCFHealthMonitoringGroup/HealthMonitorInstances" />
            <sCSPolicyUpdateDomainMoniker name="/WCFHealthMonitoring/WCFHealthMonitoringGroup/HealthMonitorUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/WCFHealthMonitoring/WCFHealthMonitoringGroup/HealthMonitorFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="HealthMonitorUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="HealthMonitorFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="HealthMonitorInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
</serviceModel>