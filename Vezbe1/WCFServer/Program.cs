﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using WCFContracts;

namespace WCFServer
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost svc = new ServiceHost(typeof(HealthMonitoring));
            svc.AddServiceEndpoint(typeof(IHealthMonitoring), new NetTcpBinding(), new Uri(@"net.tcp://localhost:33000/IHealthMonitoring"));
            svc.Open();



            Console.WriteLine("Pres enter to stop hosting");
            Console.ReadLine();
            svc.Close();
        }
    }
}
