﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WCFContracts;

namespace WCFServer
{
    public class HealthMonitoring : IHealthMonitoring
    {
        public void IAmAlive()
        {
            Trace.WriteLine("I am alive");
        }
    }
}
