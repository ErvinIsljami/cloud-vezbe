﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="StudentService" generation="1" functional="0" release="0" Id="dafa5357-f3e6-4fed-985b-d14c940ae49d" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="StudentServiceGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="StudentService_WebRole:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/StudentService/StudentServiceGroup/LB:StudentService_WebRole:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="StudentService_WebRole:DataConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/StudentService/StudentServiceGroup/MapStudentService_WebRole:DataConnectionString" />
          </maps>
        </aCS>
        <aCS name="StudentService_WebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/StudentService/StudentServiceGroup/MapStudentService_WebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="StudentService_WebRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/StudentService/StudentServiceGroup/MapStudentService_WebRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:StudentService_WebRole:Endpoint1">
          <toPorts>
            <inPortMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRole/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapStudentService_WebRole:DataConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRole/DataConnectionString" />
          </setting>
        </map>
        <map name="MapStudentService_WebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapStudentService_WebRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="StudentService_WebRole" generation="1" functional="0" release="0" software="C:\Users\Ervin\source\repos\StudentServiceWeb\StudentService\csx\Debug\roles\StudentService_WebRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="DataConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;StudentService_WebRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;StudentService_WebRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="StudentService_WebRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="StudentService_WebRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="StudentService_WebRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="33356b3a-f744-471f-9a8a-21475d6d42b4" ref="Microsoft.RedDog.Contract\ServiceContract\StudentServiceContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="4bf02058-6866-4699-8f4c-37a17e0a0b0f" ref="Microsoft.RedDog.Contract\Interface\StudentService_WebRole:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRole:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>