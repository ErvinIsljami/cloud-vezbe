﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="ProcessItemsWeb" generation="1" functional="0" release="0" Id="f89c8f0e-e8cb-4e64-b00b-9f17034a44ff" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="ProcessItemsWebGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="WebRole1:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/LB:WebRole1:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="ItemRepositoryWorker:DataConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/MapItemRepositoryWorker:DataConnectionString" />
          </maps>
        </aCS>
        <aCS name="ItemRepositoryWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/MapItemRepositoryWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="ItemRepositoryWorkerInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/MapItemRepositoryWorkerInstances" />
          </maps>
        </aCS>
        <aCS name="WebRole1:DataConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/MapWebRole1:DataConnectionString" />
          </maps>
        </aCS>
        <aCS name="WebRole1:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/MapWebRole1:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="WebRole1Instances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/MapWebRole1Instances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:WebRole1:Endpoint1">
          <toPorts>
            <inPortMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/WebRole1/Endpoint1" />
          </toPorts>
        </lBChannel>
        <sFSwitchChannel name="SW:ItemRepositoryWorker:InternalEndpoint">
          <toPorts>
            <inPortMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/ItemRepositoryWorker/InternalEndpoint" />
          </toPorts>
        </sFSwitchChannel>
      </channels>
      <maps>
        <map name="MapItemRepositoryWorker:DataConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/ItemRepositoryWorker/DataConnectionString" />
          </setting>
        </map>
        <map name="MapItemRepositoryWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/ItemRepositoryWorker/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapItemRepositoryWorkerInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/ItemRepositoryWorkerInstances" />
          </setting>
        </map>
        <map name="MapWebRole1:DataConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/WebRole1/DataConnectionString" />
          </setting>
        </map>
        <map name="MapWebRole1:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/WebRole1/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapWebRole1Instances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/WebRole1Instances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="ItemRepositoryWorker" generation="1" functional="0" release="0" software="C:\Users\Ervin\Documents\cloud-vezbe.git\Kolokvijum2Zad1\ProcessItemsWeb\csx\Debug\roles\ItemRepositoryWorker" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="InternalEndpoint" protocol="tcp" />
              <outPort name="ItemRepositoryWorker:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/SW:ItemRepositoryWorker:InternalEndpoint" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="DataConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;ItemRepositoryWorker&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;ItemRepositoryWorker&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;WebRole1&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/ItemRepositoryWorkerInstances" />
            <sCSPolicyUpdateDomainMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/ItemRepositoryWorkerUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/ItemRepositoryWorkerFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="WebRole1" generation="1" functional="0" release="0" software="C:\Users\Ervin\Documents\cloud-vezbe.git\Kolokvijum2Zad1\ProcessItemsWeb\csx\Debug\roles\WebRole1" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
              <outPort name="ItemRepositoryWorker:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/SW:ItemRepositoryWorker:InternalEndpoint" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="DataConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WebRole1&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;ItemRepositoryWorker&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;WebRole1&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/WebRole1Instances" />
            <sCSPolicyUpdateDomainMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/WebRole1UpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/WebRole1FaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="WebRole1UpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="ItemRepositoryWorkerUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="ItemRepositoryWorkerFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="WebRole1FaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="ItemRepositoryWorkerInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="WebRole1Instances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="d3f4fc23-90df-4577-9b1d-eee90c9bc001" ref="Microsoft.RedDog.Contract\ServiceContract\ProcessItemsWebContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="936f783a-d4b5-42f7-b504-267d2cf6e960" ref="Microsoft.RedDog.Contract\Interface\WebRole1:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/ProcessItemsWeb/ProcessItemsWebGroup/WebRole1:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>