﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="StudentService" generation="1" functional="0" release="0" Id="ddde3bf9-47d3-4653-b60d-d45568583888" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="StudentServiceGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="StudentService_WebRole:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/StudentService/StudentServiceGroup/LB:StudentService_WebRole:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="ImageConverter_WorkerRole:DataConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/StudentService/StudentServiceGroup/MapImageConverter_WorkerRole:DataConnectionString" />
          </maps>
        </aCS>
        <aCS name="ImageConverter_WorkerRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/StudentService/StudentServiceGroup/MapImageConverter_WorkerRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="ImageConverter_WorkerRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/StudentService/StudentServiceGroup/MapImageConverter_WorkerRoleInstances" />
          </maps>
        </aCS>
        <aCS name="StudentService_WebRole:DataConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/StudentService/StudentServiceGroup/MapStudentService_WebRole:DataConnectionString" />
          </maps>
        </aCS>
        <aCS name="StudentService_WebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/StudentService/StudentServiceGroup/MapStudentService_WebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="StudentService_WebRoleInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/StudentService/StudentServiceGroup/MapStudentService_WebRoleInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:StudentService_WebRole:Endpoint1">
          <toPorts>
            <inPortMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRole/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapImageConverter_WorkerRole:DataConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/StudentService/StudentServiceGroup/ImageConverter_WorkerRole/DataConnectionString" />
          </setting>
        </map>
        <map name="MapImageConverter_WorkerRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/StudentService/StudentServiceGroup/ImageConverter_WorkerRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapImageConverter_WorkerRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/StudentService/StudentServiceGroup/ImageConverter_WorkerRoleInstances" />
          </setting>
        </map>
        <map name="MapStudentService_WebRole:DataConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRole/DataConnectionString" />
          </setting>
        </map>
        <map name="MapStudentService_WebRole:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRole/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapStudentService_WebRoleInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRoleInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="ImageConverter_WorkerRole" generation="1" functional="0" release="0" software="C:\Users\Ervin\Documents\cloud-vezbe.git\Vezbe7\StudentService\csx\Debug\roles\ImageConverter_WorkerRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="DataConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;ImageConverter_WorkerRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;ImageConverter_WorkerRole&quot; /&gt;&lt;r name=&quot;StudentService_WebRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/StudentService/StudentServiceGroup/ImageConverter_WorkerRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/StudentService/StudentServiceGroup/ImageConverter_WorkerRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/StudentService/StudentServiceGroup/ImageConverter_WorkerRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="StudentService_WebRole" generation="1" functional="0" release="0" software="C:\Users\Ervin\Documents\cloud-vezbe.git\Vezbe7\StudentService\csx\Debug\roles\StudentService_WebRole" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="DataConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;StudentService_WebRole&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;ImageConverter_WorkerRole&quot; /&gt;&lt;r name=&quot;StudentService_WebRole&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRoleInstances" />
            <sCSPolicyUpdateDomainMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRoleUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRoleFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="StudentService_WebRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="ImageConverter_WorkerRoleUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="ImageConverter_WorkerRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="StudentService_WebRoleFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="ImageConverter_WorkerRoleInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="StudentService_WebRoleInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="44da7c4b-2bf2-4637-bddc-c5919a946c77" ref="Microsoft.RedDog.Contract\ServiceContract\StudentServiceContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="a09fd4dd-7194-4895-9067-7d305a8293a3" ref="Microsoft.RedDog.Contract\Interface\StudentService_WebRole:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/StudentService/StudentServiceGroup/StudentService_WebRole:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>