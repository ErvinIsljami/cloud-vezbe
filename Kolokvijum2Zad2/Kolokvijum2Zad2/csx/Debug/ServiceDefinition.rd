﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="Kolokvijum2Zad2" generation="1" functional="0" release="0" Id="dfb8a29f-a4f4-4ff7-aa56-04b7c637cf00" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="Kolokvijum2Zad2Group" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="WebSimulator:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/LB:WebSimulator:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="JobWorker:DataConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/MapJobWorker:DataConnectionString" />
          </maps>
        </aCS>
        <aCS name="JobWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/MapJobWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="JobWorkerInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/MapJobWorkerInstances" />
          </maps>
        </aCS>
        <aCS name="WebSimulator:DataConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/MapWebSimulator:DataConnectionString" />
          </maps>
        </aCS>
        <aCS name="WebSimulator:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/MapWebSimulator:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="WebSimulatorInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/MapWebSimulatorInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:WebSimulator:Endpoint1">
          <toPorts>
            <inPortMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/WebSimulator/Endpoint1" />
          </toPorts>
        </lBChannel>
        <sFSwitchChannel name="SW:JobWorker:InternalEndpoint">
          <toPorts>
            <inPortMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/JobWorker/InternalEndpoint" />
          </toPorts>
        </sFSwitchChannel>
        <sFSwitchChannel name="SW:JobWorker:InternalEndpoint2">
          <toPorts>
            <inPortMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/JobWorker/InternalEndpoint2" />
          </toPorts>
        </sFSwitchChannel>
      </channels>
      <maps>
        <map name="MapJobWorker:DataConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/JobWorker/DataConnectionString" />
          </setting>
        </map>
        <map name="MapJobWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/JobWorker/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapJobWorkerInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/JobWorkerInstances" />
          </setting>
        </map>
        <map name="MapWebSimulator:DataConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/WebSimulator/DataConnectionString" />
          </setting>
        </map>
        <map name="MapWebSimulator:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/WebSimulator/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapWebSimulatorInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/WebSimulatorInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="JobWorker" generation="1" functional="0" release="0" software="C:\Users\Ervin\Documents\cloud-vezbe.git\Kolokvijum2Zad2\Kolokvijum2Zad2\csx\Debug\roles\JobWorker" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="InternalEndpoint" protocol="tcp" />
              <inPort name="InternalEndpoint2" protocol="tcp" />
              <outPort name="JobWorker:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/SW:JobWorker:InternalEndpoint" />
                </outToChannel>
              </outPort>
              <outPort name="JobWorker:InternalEndpoint2" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/SW:JobWorker:InternalEndpoint2" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="DataConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;JobWorker&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;JobWorker&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;e name=&quot;InternalEndpoint2&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;WebSimulator&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/JobWorkerInstances" />
            <sCSPolicyUpdateDomainMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/JobWorkerUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/JobWorkerFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
        <groupHascomponents>
          <role name="WebSimulator" generation="1" functional="0" release="0" software="C:\Users\Ervin\Documents\cloud-vezbe.git\Kolokvijum2Zad2\Kolokvijum2Zad2\csx\Debug\roles\WebSimulator" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
              <outPort name="JobWorker:InternalEndpoint" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/SW:JobWorker:InternalEndpoint" />
                </outToChannel>
              </outPort>
              <outPort name="JobWorker:InternalEndpoint2" protocol="tcp">
                <outToChannel>
                  <sFSwitchChannelMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/SW:JobWorker:InternalEndpoint2" />
                </outToChannel>
              </outPort>
            </componentports>
            <settings>
              <aCS name="DataConnectionString" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;WebSimulator&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;JobWorker&quot;&gt;&lt;e name=&quot;InternalEndpoint&quot; /&gt;&lt;e name=&quot;InternalEndpoint2&quot; /&gt;&lt;/r&gt;&lt;r name=&quot;WebSimulator&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/WebSimulatorInstances" />
            <sCSPolicyUpdateDomainMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/WebSimulatorUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/WebSimulatorFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="WebSimulatorUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyUpdateDomain name="JobWorkerUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="JobWorkerFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyFaultDomain name="WebSimulatorFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="JobWorkerInstances" defaultPolicy="[1,1,1]" />
        <sCSPolicyID name="WebSimulatorInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="06c99ad4-9ed5-4319-b810-9a890d48d8be" ref="Microsoft.RedDog.Contract\ServiceContract\Kolokvijum2Zad2Contract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="123fd272-54ab-4a37-acb4-dd2ae264b0f2" ref="Microsoft.RedDog.Contract\Interface\WebSimulator:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/Kolokvijum2Zad2/Kolokvijum2Zad2Group/WebSimulator:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>